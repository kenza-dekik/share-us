const express =require('express');
const mongoose= require('mongoose');
const bodyParser= require('body-parser');
const morgan= require('morgan');

require('dotenv/config');

let app = express(); 

app.use(express.json());
app.use(bodyParser.json());




//endpoint 
app.get('/',(req,res)=>{
    res.send('hello world')
})

//add morgan to log HTPP requests
app.use(morgan('combined'))
mongoose.connect(process.env.DATA_BASE,
{useNewUrlParser:true},()=>{
    console.log(`connecte to the database ${process.env.DATA_BASE}`)
})

let port= process.env.PORT||3002;
app.listen(port,()=>{
    console.log(`you are listening to ${port}`);
})